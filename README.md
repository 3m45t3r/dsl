# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Collaboration DSL
* 1.0.0-SNAPSHOT

### How do I get set up? ###

### 1. Prerequisites

You need to install:

* Eclipse Xtext http://www.eclipse.org/Xtext/download.html
* Java 1.5 or higher
* Maven, tested with 3.1.1 and 3.2.1(eclipse-embedded)

### 2. Setup

```
git clone https://bitbucket.org/3m45t3r/dsl.git
 cd dsl/main/
 mvn clean install
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact