package at.tuwien.dsg.ui.contentassist.util;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import at.tuwien.dsg.dsl.Binding;
import at.tuwien.dsg.dsl.Step;
import at.tuwien.dsg.dsl.StepInterface;

public class NameCrawler {

	private NameCrawler() {
	}

	public static Set<String> availableBindingNames(EObject step) {
		Set<String> names = new HashSet<String>();
		while (step != null) {
			if (step instanceof Step) {
				Step s = (Step) step;
				names.addAll(interfaceNames(s.getInterfaces()));
				names.addAll(bindingNames(s.getBindings()));
			}
			step = step.eContainer();
		}
		return names;
	}

	private static Set<String> bindingNames(EList<Binding> bindings) {
		Set<String> result = new HashSet<String>();
		for (Binding b : bindings) {
			result.add(b.getRight());
			result.add(b.getLeft());
		}
		return result;
	}

	private static Set<String> interfaceNames(EList<StepInterface> interfaces) {
		Set<String> result = new HashSet<String>();
		for (StepInterface si : interfaces) {
			result.add(si.getName());
		}
		return result;
	}
}
