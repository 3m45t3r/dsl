package at.tuwien.dsg.dsl.tests.parser

import at.tuwien.dsg.DslInjectorProvider
import at.tuwien.dsg.dsl.HadlTypedExceptionHandler
import at.tuwien.dsg.dsl.HadlTypedStepInterface
import at.tuwien.dsg.dsl.HandlerType
import at.tuwien.dsg.dsl.InBinding
import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.OutBinding
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepType
import javax.inject.Inject
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import at.tuwien.dsg.dsl.JavaTypedStepInterface
import at.tuwien.dsg.dsl.JavaTypedExceptionHandler
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.LittleJilModel

@InjectWith(DslInjectorProvider)
@RunWith(XtextRunner)
class LittleJilParserTest {
	@Inject extension ParseHelper<Model>

	@Test
	def void testSimpleSequential() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Sequential name SeqStep
		end
	)'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.SEQUENTIAL, step.kind)
		Assert::assertEquals("SeqStep", step.name)
	}

	@Test
	def void testSimpleTry() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Try name TryStep
		end
	)'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.TRY, step.kind)
		Assert::assertEquals("TryStep", step.name)
	}

	@Test
	def void testSimpleLeaf() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Leaf name LeafStep
		end
	)'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.LEAF, step.kind)
		Assert::assertEquals("LeafStep", step.name)
	}

	@Test
	def void testSimpleReference() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			TaskReference name Reference
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.TASK_REFERENCE, step.kind)
		Assert::assertEquals("Reference", step.name)
	}

	@Test
	def void testSimpleChoice() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Choice name ChoiceStep
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.CHOICE, step.kind)
		Assert::assertEquals("ChoiceStep", step.name)
	}

	@Test
	def void testSimpleParallel() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name ParallelStep
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.PARALLEL, step.kind)
		Assert::assertEquals("ParallelStep", step.name)
	}

	@Test
	def void testInterfaceDeclaration() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name ParallelStep
			interfaces 
				[Resource r1 hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterIn p1 hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterOut p2 hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterInOut p3 hADLType hadlmodel.hadlStructure.Comp1]
				[Local l1 hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterIn j1 jvmType java.lang.String]
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		val comp1 = hadlStruct.elements.get(0)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val step = littleJilModel.steps.get(0) as Step
		Assert::assertEquals(StepType.PARALLEL, step.kind)
		Assert::assertEquals("ParallelStep", step.name)
		Assert::assertEquals(6, step.interfaces.size)

		Assert::assertTrue(step.interfaces.get(0) instanceof HadlTypedStepInterface)
		val resource = step.interfaces.get(0) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.RESOURCE, resource.kind)
		Assert::assertEquals("r1", resource.name)
		Assert::assertEquals(comp1, resource.type)

		Assert::assertTrue(step.interfaces.get(1) instanceof HadlTypedStepInterface)
		val pIn = step.interfaces.get(1) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_IN, pIn.kind)
		Assert::assertEquals("p1", pIn.name)
		Assert::assertEquals(comp1, pIn.type)

		Assert::assertTrue(step.interfaces.get(2) instanceof HadlTypedStepInterface)
		val pOut = step.interfaces.get(2) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_OUT, pOut.kind)
		Assert::assertEquals("p2", pOut.name)
		Assert::assertEquals(comp1, pOut.type)

		Assert::assertTrue(step.interfaces.get(3) instanceof HadlTypedStepInterface)
		val pInOut = step.interfaces.get(3) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_IN_OUT, pInOut.kind)
		Assert::assertEquals("p3", pInOut.name)
		Assert::assertEquals(comp1, pInOut.type)
		
		Assert::assertTrue(step.interfaces.get(4) instanceof HadlTypedStepInterface)
		val l1 = step.interfaces.get(4) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.LOCAL, l1.kind)
		Assert::assertEquals("l1", l1.name)
		Assert::assertEquals(comp1, l1.type)
		
		Assert::assertTrue(step.interfaces.get(5) instanceof JavaTypedStepInterface)
		val ji = step.interfaces.get(5) as JavaTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_IN, ji.kind)
		Assert::assertEquals("j1", ji.name)
		Assert::assertEquals("java.lang.String", ji.type.qualifiedName)
	}

	@Test
	def void testParentChildStep() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
			interfaces 
				[Resource agent hADLType hadlmodel.hadlStructure.Comp1]
			steps (
				Sequential name Child
				
				Sequential name Card1
					cardinality 0,*
				Sequential name Card2
					cardinality 1,2
				Sequential name Card3
					cardinality 1,*
			)
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(1, hadlStruct.elements.size)
		val comp1 = hadlStruct.elements.get(0)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val rootStep = littleJilModel.steps.get(0) as Step
		Assert::assertEquals("Parent", rootStep.name)
		Assert::assertEquals(StepType.PARALLEL, rootStep.kind)
		Assert::assertEquals(1, rootStep.interfaces.size)
		Assert::assertTrue(rootStep.interfaces.get(0) instanceof HadlTypedStepInterface)
		val rootInterface = rootStep.interfaces.get(0) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.RESOURCE, rootInterface.kind)
		Assert::assertEquals("agent", rootInterface.name)
		Assert::assertEquals(comp1, rootInterface.type)
		Assert::assertEquals(4, rootStep.substeps.size)
		
		val child = rootStep.substeps.get(0)
		Assert::assertEquals("Child", child.name)
		Assert::assertEquals(StepType.SEQUENTIAL, child.kind)
		Assert::assertNull(child.cardinality)
		
		val card1 = rootStep.substeps.get(1)
		Assert::assertEquals("Card1", card1.name)
		Assert::assertEquals(StepType.SEQUENTIAL, card1.kind)
		Assert::assertNotNull(card1.cardinality)
		Assert::assertEquals("0", card1.cardinality.lowerBound)
		Assert::assertEquals("*", card1.cardinality.upperBound)
		
		val card2 = rootStep.substeps.get(2)
		Assert::assertEquals("Card2", card2.name)
		Assert::assertEquals(StepType.SEQUENTIAL, card2.kind)
		Assert::assertNotNull(card2.cardinality)
		Assert::assertEquals("1", card2.cardinality.lowerBound)
		Assert::assertEquals("2", card2.cardinality.upperBound)
		
		val card3 = rootStep.substeps.get(3)
		Assert::assertEquals("Card3", card3.name)
		Assert::assertEquals(StepType.SEQUENTIAL, card3.kind)
		Assert::assertNotNull(card3.cardinality)
		Assert::assertEquals("1", card3.cardinality.lowerBound)
		Assert::assertEquals("*", card3.cardinality.upperBound)
	}

	@Test
	def void testExceptionHandler() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Message name ExceptionType1
				Message name ExceptionType2
				Message name ExceptionType3
				Message name ExceptionType4
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
				handlers
					[name ChoiceHandler hADLType hadlmodel.hadlStructure.ExceptionType1 action Continue]
					[name SeqHandler hADLType hadlmodel.hadlStructure.ExceptionType2 action Complete]
					[name ParaHandler hADLType hadlmodel.hadlStructure.ExceptionType3 action Rethrow]
					[name TryHandler hADLType hadlmodel.hadlStructure.ExceptionType4 action Restart]
					[name JavaHandler jvmType java.util.logging.Handler action Continue]
				steps (
					Choice name ChoiceStep throws [ChoiceExc handeld by ChoiceHandler]
					Sequential name SeqStep throws [SeqExc handeld by SeqHandler]
					Parallel name ParaStep throws [ParaExc handeld by ParaHandler]
					Try name TryStep throws [TryExc handeld by TryHandler]
				)
		end
	)
'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(4, hadlStruct.elements.size)
		val excp1 = hadlStruct.elements.get(0)
		val excp2 = hadlStruct.elements.get(1)
		val excp3 = hadlStruct.elements.get(2)
		val excp4 = hadlStruct.elements.get(3)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val rootStep = littleJilModel.steps.get(0) as Step

		Assert::assertEquals("Parent", rootStep.name)
		Assert::assertEquals(StepType.PARALLEL, rootStep.kind)

		Assert::assertEquals(5, rootStep.exceptionHandlers.size)

		Assert::assertTrue(rootStep.exceptionHandlers.get(0) instanceof HadlTypedExceptionHandler)
		val h1 = rootStep.exceptionHandlers.get(0) as HadlTypedExceptionHandler
		Assert::assertEquals("ChoiceHandler", h1.name)
		Assert::assertEquals(HandlerType.CONTINUE, h1.action)
		Assert::assertEquals(excp1, h1.type)

		Assert::assertTrue(rootStep.exceptionHandlers.get(1) instanceof HadlTypedExceptionHandler)
		val h2 = rootStep.exceptionHandlers.get(1) as HadlTypedExceptionHandler
		Assert::assertEquals("SeqHandler", h2.name)
		Assert::assertEquals(HandlerType.COMPLETE, h2.action)
		Assert::assertEquals(excp2, h2.type)

		Assert::assertTrue(rootStep.exceptionHandlers.get(2) instanceof HadlTypedExceptionHandler)
		val h3 = rootStep.exceptionHandlers.get(2) as HadlTypedExceptionHandler
		Assert::assertEquals("ParaHandler", h3.name)
		Assert::assertEquals(HandlerType.RETHROW, h3.action)
		Assert::assertEquals(excp3, h3.type)

		Assert::assertTrue(rootStep.exceptionHandlers.get(3) instanceof HadlTypedExceptionHandler)
		val h4 = rootStep.exceptionHandlers.get(3) as HadlTypedExceptionHandler
		Assert::assertEquals("TryHandler", h4.name)
		Assert::assertEquals(HandlerType.RESTART, h4.action)
		Assert::assertEquals(excp4, h4.type)
		
		Assert::assertTrue(rootStep.exceptionHandlers.get(4) instanceof JavaTypedExceptionHandler)
		val h5 = rootStep.exceptionHandlers.get(4) as JavaTypedExceptionHandler
		Assert::assertEquals("JavaHandler", h5.name)
		Assert::assertEquals(HandlerType.CONTINUE, h5.action)
		Assert::assertEquals("java.util.logging.Handler", h5.type.qualifiedName)

		Assert::assertEquals(4, rootStep.substeps.size)

		val choiceStep = rootStep.substeps.get(0)
		Assert::assertEquals("ChoiceStep", choiceStep.name)
		Assert::assertEquals(StepType.CHOICE, choiceStep.kind)
		Assert::assertEquals(1, choiceStep.stepExceptions.size)
		Assert::assertEquals("ChoiceExc", choiceStep.stepExceptions.get(0).name)
		Assert::assertEquals(h1, choiceStep.stepExceptions.get(0).handeldBy)

		val seqStep = rootStep.substeps.get(1)
		Assert::assertEquals("SeqStep", seqStep.name)
		Assert::assertEquals(StepType.SEQUENTIAL, seqStep.kind)
		Assert::assertEquals(1, seqStep.stepExceptions.size)
		Assert::assertEquals("SeqExc", seqStep.stepExceptions.get(0).name)
		Assert::assertEquals(h2, seqStep.stepExceptions.get(0).handeldBy)

		val paraStep = rootStep.substeps.get(2)
		Assert::assertEquals("ParaStep", paraStep.name)
		Assert::assertEquals(StepType.PARALLEL, paraStep.kind)
		Assert::assertEquals(1, paraStep.stepExceptions.size)
		Assert::assertEquals("ParaExc", paraStep.stepExceptions.get(0).name)
		Assert::assertEquals(h3, paraStep.stepExceptions.get(0).handeldBy)

		val tryStep = rootStep.substeps.get(3)
		Assert::assertEquals("TryStep", tryStep.name)
		Assert::assertEquals(StepType.TRY, tryStep.kind)
		Assert::assertEquals(1, tryStep.stepExceptions.size)
		Assert::assertEquals("TryExc", tryStep.stepExceptions.get(0).name)
		Assert::assertEquals(h4, tryStep.stepExceptions.get(0).handeldBy)
	}

	@Test
	def void testParentChildStepWithBinding() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
				
				Message name Obj1
				
				
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
			interfaces 
				[Resource agent hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterIn parentResult hADLType hadlmodel.hadlStructure.Obj1]
			steps (
				Sequential name Child
				 bindings 
				 	[childAgent < agent ]
				 	[result > parentResult]
			)
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(2, hadlStruct.elements.size)
		val comp1 = hadlStruct.elements.get(0)
		val obj1 = hadlStruct.elements.get(1)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val rootStep = littleJilModel.steps.get(0) as Step

		Assert::assertEquals("Parent", rootStep.name)
		Assert::assertEquals(StepType.PARALLEL, rootStep.kind)
		Assert::assertEquals(2, rootStep.interfaces.size)

		Assert::assertTrue(rootStep.interfaces.get(0) instanceof HadlTypedStepInterface)
		val resource = rootStep.interfaces.get(0) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.RESOURCE, resource.kind)
		Assert::assertEquals("agent", resource.name)
		Assert::assertEquals(comp1, resource.type)

		Assert::assertTrue(rootStep.interfaces.get(1) instanceof HadlTypedStepInterface)
		val paramIn = rootStep.interfaces.get(1) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_IN, paramIn.kind)
		Assert::assertEquals("parentResult", paramIn.name)
		Assert::assertEquals(obj1, paramIn.type)

		Assert::assertEquals(1, rootStep.substeps.size)
		val childStep = rootStep.substeps.get(0)
		Assert::assertEquals("Child", childStep.name)
		Assert::assertEquals(StepType.SEQUENTIAL, childStep.kind)
		Assert::assertEquals(2, childStep.bindings.size)
		val binding1 = childStep.bindings.get(0)
		Assert::assertTrue(binding1 instanceof InBinding)
		Assert::assertEquals("childAgent", binding1.left)
		Assert::assertEquals("agent", binding1.right)
		val binding2 = childStep.bindings.get(1)
		Assert::assertTrue(binding2 instanceof OutBinding)
		Assert::assertEquals("result", binding2.left)
		Assert::assertEquals("parentResult", binding2.right)
	}

	@Test
	def void testSmallTreeWithBinding() {
		val model = '''
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
				
				Message name Obj1
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
			interfaces 
				[Resource agent hADLType hadlmodel.hadlStructure.Comp1]
				[ParameterIn parentResult hADLType hadlmodel.hadlStructure.Obj1]
			steps (
				Sequential name Child1
				 bindings 
				 	[childAgent < agent ]
				 	[result > parentResult]
				 
				 Parallel name Child2
				 bindings 
				 	[childAgent1 < agent ]
				 steps (
				  Try name Child21
				  bindings 
				  	[childAgent2 < childAgent1 ]
				  
				  Choice name Child22
				  bindings 
				  	[childAgent3 < childAgent1 ]
				 )
			)
		end
	)
		'''.parse
		Assert::assertEquals(2, model.models.size)
		Assert::assertTrue(model.models.get(0) instanceof HadlModel)
		val hadlModel = model.models.get(0) as HadlModel
		Assert::assertEquals("hadlmodel", hadlModel.name)
		Assert::assertEquals(1, hadlModel.structures.size)
		val hadlStruct = hadlModel.structures.get(0)
		Assert::assertEquals("hadlStructure", hadlStruct.name)
		Assert::assertEquals(2, hadlStruct.elements.size)
		val comp1 = hadlStruct.elements.get(0)
		val obj1 = hadlStruct.elements.get(1)
		
		Assert::assertTrue(model.models.get(1) instanceof LittleJilModel)
		val littleJilModel = model.models.get(1) as LittleJilModel
		Assert::assertEquals("ljmodel", littleJilModel.name)
		Assert::assertEquals(1, littleJilModel.steps.size)
		val rootStep = littleJilModel.steps.get(0) as Step

		Assert::assertEquals("Parent", rootStep.name)
		Assert::assertEquals(StepType.PARALLEL, rootStep.kind)
		Assert::assertEquals(2, rootStep.interfaces.size)

		Assert::assertTrue(rootStep.interfaces.get(0) instanceof HadlTypedStepInterface)
		val resource = rootStep.interfaces.get(0) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.RESOURCE, resource.kind)
		Assert::assertEquals("agent", resource.name)
		Assert::assertEquals(comp1, resource.type)

		Assert::assertTrue(rootStep.interfaces.get(1) instanceof HadlTypedStepInterface)
		val paramIn = rootStep.interfaces.get(1) as HadlTypedStepInterface
		Assert::assertEquals(InterfaceType.PARAMETER_IN, paramIn.kind)
		Assert::assertEquals("parentResult", paramIn.name)
		Assert::assertEquals(obj1, paramIn.type)

		Assert::assertEquals(2, rootStep.substeps.size)
		val childStep1 = rootStep.substeps.get(0)
		Assert::assertTrue(childStep1.substeps.empty)
		Assert::assertEquals("Child1", childStep1.name)
		Assert::assertEquals(StepType.SEQUENTIAL, childStep1.kind)
		Assert::assertEquals(2, childStep1.bindings.size)
		val binding1 = childStep1.bindings.get(0)
		Assert::assertTrue(binding1 instanceof InBinding)
		Assert::assertEquals("childAgent", binding1.left)
		Assert::assertEquals("agent", binding1.right)
		val binding2 = childStep1.bindings.get(1)
		Assert::assertTrue(binding2 instanceof OutBinding)
		Assert::assertEquals("result", binding2.left)
		Assert::assertEquals("parentResult", binding2.right)

		val childStep2 = rootStep.substeps.get(1)
		Assert::assertEquals("Child2", childStep2.name)
		Assert::assertEquals(StepType.PARALLEL, childStep2.kind)
		Assert::assertEquals(1, childStep2.bindings.size)

		Assert::assertEquals(2, childStep2.substeps.size)
		val tryStep = childStep2.substeps.get(0)
		Assert::assertEquals("Child21", tryStep.name)
		Assert::assertEquals(StepType.TRY, tryStep.kind)
		Assert::assertEquals(1, tryStep.bindings.size)

		val choiceStep = childStep2.substeps.get(1)
		Assert::assertEquals("Child22", choiceStep.name)
		Assert::assertEquals(StepType.CHOICE, choiceStep.kind)
		Assert::assertEquals(1, choiceStep.bindings.size)
	}
}
