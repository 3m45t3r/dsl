package at.tuwien.dsg.dsl.tests.generator

import at.tuwien.dsg.DslInjectorProvider
import at.tuwien.dsg.dsl.Model
import javax.inject.Inject
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import at.tuwien.dsg.dsl.tests.generator.util.HadlOutputValidator
import at.tuwien.dsg.dsl.tests.generator.util.DslXmlFormatter
import at.tuwien.dsg.dsl.tests.generator.util.XmlFileReader

@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DslInjectorProvider))
class HadlGeneratorTest {
	@Inject IGenerator generator;
	@Inject extension ParseHelper<Model>

	def String validateAgainstSchema(Model model, String packageName) {
		val fsa = new InMemoryFileSystemAccess();
		generator.doGenerate(model.eResource, fsa)
		Assert::assertEquals(2, fsa.textFiles.size)
		Assert::assertTrue(fsa.textFiles.containsKey(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-hADL.xml"))
		Assert::assertTrue(fsa.textFiles.containsKey(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-littlejil.xml"))
		val output = fsa.textFiles.get(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-hADL.xml");
		Assert::assertTrue(HadlOutputValidator.validate(output))
		return output.toString
	}

	@Test
	def void generateSeveralStrucutres() {
		val model = '''
package severalStructures
models
	( 	name testmodel
		structures
			name structure1
			elements
				Artifact name Artifact1
			end
			name structure2
			elements
				Message name Message1
			end
			name structure3
			elements
				Stream name Stream1
			end
		end
	)
'''.
			parse
		val output = validateAgainstSchema(model, "severalStructures")
		val ref = XmlFileReader.read("/hadl/severalStructures.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}

	@Test
	def void simpleGeneration() {
		val model = '''
package simple
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
				Message name Obj1
			end
		end
	)
		'''.
			parse
		val output = validateAgainstSchema(model, "simple")
		val ref = XmlFileReader.read("/hadl/simple.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))

	}

	@Test
	def void generateCollabLink() {
		val model = '''package collabLink
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name WikiCompositeObject actions
				[EditingWiki type {C R U D}]
				Artifact name WikiPage actions 
				[read]
				Link name Link1 testmodel.teststructure.WikiPage.read : testmodel.teststructure.WikiCompositeObject.EditingWiki
				Link name Link2 testmodel.teststructure.WikiPage.read : testmodel.teststructure.WikiCompositeObject.EditingWiki
			end
		end
	)
'''.
			parse
		val output = validateAgainstSchema(model, "collabLink")
		val ref = XmlFileReader.read("/hadl/collabLink.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}

	@Test
	def void generateObjectRef() {
		val model = '''
package objectRef
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Artifact name WikiPage actions 
				[read]
				Artifact name SubPage
			end
			relations
				[name ParentChild : testmodel.teststructure.SubPage inherits from testmodel.teststructure.WikiPage]
				[name Inclusion : testmodel.teststructure.WikiPage contains testmodel.teststructure.SubPage]
				[name Dependency : testmodel.teststructure.SubPage depends on testmodel.teststructure.WikiPage]
				[name Ref : testmodel.teststructure.SubPage references testmodel.teststructure.WikiPage]
			end
		end
	)
		'''.
			parse
		val output = validateAgainstSchema(model, "objectRef")
		val ref = XmlFileReader.read("/hadl/objectRef.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}

	@Test
	def void generateActionCardinality() {
		val model = '''
package actionCardinality
models
	( 	name testmodel
		structures
			name teststructure
			elements 
					Artifact name Obj1 actions
						[a1 type {R} cardinality 0,1]
						[a2 type {C R} cardinality 1,*] 
						[a3 type {C R U D} cardinality 0,*] 
			end
		end
	)
		'''.
			parse
		val output = validateAgainstSchema(model, "actionCardinality")
		val ref = XmlFileReader.read("/hadl/actionCardinality.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}

	@Test
	def void generateActivityScope() {
		val model = '''
package activityScope
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name WikiComponent actions
				[EditingWiki type {C R U D}]
				Artifact name WikiPage actions 
				[read]
				Artifact name WikiSubPage actions 
				[read]
				Link name Link1 testmodel.teststructure.WikiPage.read : testmodel.teststructure.WikiComponent.EditingWiki
				Link name Link2 testmodel.teststructure.WikiSubPage.read : testmodel.teststructure.WikiComponent.EditingWiki
				
			end
			activityScopes
				[	links 
						testmodel.teststructure.Link1
					collaborators
						testmodel.teststructure.WikiComponent
					objects
						testmodel.teststructure.WikiPage
					cardinality 1,2
				]
				[	links 
						testmodel.teststructure.Link1
						testmodel.teststructure.Link2
					collaborators
						testmodel.teststructure.WikiComponent
					objects
						testmodel.teststructure.WikiPage
						testmodel.teststructure.WikiSubPage
					cardinality 0,*
				]
			end
		end
	)
'''.
			parse
		val output = validateAgainstSchema(model, "activityScope")
		val ref = XmlFileReader.read("/hadl/activityScope.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}

	@Test
	def void generateSubstructure() {
		val model = '''
package psubstructure
models
	( 	name testmodel
		structures
			name mediawiki
			elements 
				Artifact name WikiPage actions
					[page_read]
					[page_edit]
			end
			
			name VirtualWiki
			elements
				Artifact name VWiki actions
					[virtual_wikireading]
					[virtual_wikiediting]
					substructure reference testmodel.mediawiki virtual true
					wires
						Wire testmodel.mediawiki.WikiPage.page_read : testmodel.VirtualWiki.VWiki.virtual_wikireading
						Wire testmodel.mediawiki.WikiPage.page_edit : testmodel.VirtualWiki.VWiki.virtual_wikiediting
			end
		end
	)
		'''.
			parse
		val output = validateAgainstSchema(model, "psubstructure")
		val ref = XmlFileReader.read("/hadl/psubstructure.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(output))
	}
}
