package at.tuwien.dsg.dsl.tests.generator.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

public class HadlOutputValidator {

	private HadlOutputValidator() {
	}

	private static Logger LOG = Logger.getLogger(HadlOutputValidator.class);

	public static boolean validate(CharSequence output) throws IOException, SAXException {
		String out = output.toString();
		Source xml = new StreamSource(new StringReader(out));
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(HadlOutputValidator.class.getResource("/hADLcore.xsd"));
		Validator validator = schema.newValidator();
		try {
			validator.validate(xml);
			return true;
		} catch (SAXException e) {
			LOG.error(e.getLocalizedMessage());
		}
		return false;
	}

}
