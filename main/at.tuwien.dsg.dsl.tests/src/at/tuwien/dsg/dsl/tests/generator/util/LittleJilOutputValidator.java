package at.tuwien.dsg.dsl.tests.generator.util;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class LittleJilOutputValidator {
	private static Logger LOG = Logger.getLogger(LittleJilOutputValidator.class);

	private LittleJilOutputValidator() {
	}

	public static boolean validate(CharSequence output) throws ParserConfigurationException {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setValidating(true);
		DocumentBuilder builder = domFactory.newDocumentBuilder();
		builder.setErrorHandler(new ErrorHandler() {
			public void error(SAXParseException exception) throws SAXException {
				LOG.error(exception.getLocalizedMessage());
				throw exception;
			}

			public void fatalError(SAXParseException exception) throws SAXException {
				LOG.fatal(exception.getLocalizedMessage());
				throw exception;
			}

			public void warning(SAXParseException exception) throws SAXException {
                LOG.warn(exception.getLocalizedMessage());
				throw exception;
			}
		});
		try {
			String out = output.toString();
			out = out.replaceFirst("DOCTYPE littlejil PUBLIC \".*\"",
					"DOCTYPE littlejil SYSTEM \"schema/littlejil.dtd\"");
			// now out is rdy 4 validation
			InputSource xml = new InputSource(new StringReader(out));
			builder.parse(xml);
			return true;
		} catch (SAXException e) {
			LOG.error(e.getLocalizedMessage());
		} catch(IOException e){
            LOG.error(e.getLocalizedMessage());
        }
        return false;
	}
}
