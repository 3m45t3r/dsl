package at.tuwien.dsg.dsl.tests.generator.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;

public class XmlFileReader {

	public static String read(String filename) throws URISyntaxException, IOException {
		File file;
		URL rUrl = XmlFileReader.class.getResource(filename);
		if (rUrl.toURI().toString().startsWith("bundleresource")) {
			//if running surefire
			file = new File(FileLocator.resolve(rUrl).toURI());
		} else {
			//if running junit
			file = new File(XmlFileReader.class.getResource(filename).toURI());
		}
		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			buffer.append(readData);
		}
		reader.close();
		return buffer.toString().trim();
	}
}
