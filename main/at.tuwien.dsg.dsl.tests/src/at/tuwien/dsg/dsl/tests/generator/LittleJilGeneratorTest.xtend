package at.tuwien.dsg.dsl.tests.generator

import at.tuwien.dsg.DslInjectorProvider
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.tests.generator.util.DslXmlFormatter
import at.tuwien.dsg.dsl.tests.generator.util.LittleJilOutputValidator
import at.tuwien.dsg.dsl.tests.generator.util.XmlFileReader
import javax.inject.Inject
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import org.eclipse.xtext.junit4.InjectWith
import org.eclipse.xtext.junit4.XtextRunner
import org.eclipse.xtext.junit4.util.ParseHelper
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

//TODO add validator before parsing
@RunWith(typeof(XtextRunner))
@InjectWith(typeof(DslInjectorProvider))
class LittleJilGeneratorTest {
	@Inject IGenerator generator;
	@Inject extension ParseHelper<Model>

	def String validateAgainstSchema(Model model, String packageName) {
		val fsa = new InMemoryFileSystemAccess();
		generator.doGenerate(model.eResource, fsa)
		Assert::assertEquals(2, fsa.textFiles.size)
		Assert::assertTrue(fsa.textFiles.containsKey(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-hADL.xml"))
		Assert::assertTrue(fsa.textFiles.containsKey(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-littlejil.xml"))
		val output = fsa.textFiles.get(IFileSystemAccess::DEFAULT_OUTPUT + packageName + "-littlejil.xml")
		val result = LittleJilOutputValidator.validate(output)
		Assert::assertTrue(result)
		return output.toString
	}
	
	@Test
	def void testCardinality(){
		val model = '''
package ljcardinality
models
	(	name hadlmodel
		structures
			name hadlStructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
			interfaces 
				[Resource agent hADLType hadlmodel.hadlStructure.Comp1]
			steps (
				Sequential name Card1
					cardinality 0,*
				Sequential name Card2
					cardinality 1,2
				Sequential name Card3
					cardinality 1,*
			)
		end
	)
		'''.parse
		var result = validateAgainstSchema(model, "ljcardinality")
		val ref = XmlFileReader.read("/littlejil/ljcardinality.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(result))
	}

	@Test
	def void testMixed() {
		val model = '''
package mixed
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Message name ExceptionType1
				Message name ParameterType
				Component name Comp1 actions
						[a1 type {C R U D}]
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name HandlerStep
			handlers
				[name SeqHandler hADLType testmodel.teststructure.ExceptionType1 action Continue]
				[name JavaHandler jvmType java.util.logging.Handler action Restart]
			interfaces  
				[Resource a hADLType testmodel.teststructure.ParameterType]
				[Resource agent hADLType testmodel.teststructure.Comp1]
				[Resource jRes jvmType java.lang.String]
			steps (
				TaskReference name AReference
				Leaf name ALeaf
				Sequential name PropExStep
				bindings
					[b < a]
				steps (
					Sequential name ThrowStep throws [SeqExc handeld by SeqHandler]
				)
			)
		end
	)
'''.
			parse
		val result = validateAgainstSchema(model, "mixed")
		val ref = XmlFileReader.read("/littlejil/mixed.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(result))
	}

	@Test
	def void testBindings() {
		val model = '''package ljbindings
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Component name Comp1 actions
				[a1 type {C R U D}]
				Message name Obj1
				Artifact name Obj2
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name Parent
			interfaces 
				[Resource agent hADLType testmodel.teststructure.Comp1]
				[ParameterIn parentResult hADLType testmodel.teststructure.Obj1]
				[ParameterInOut inOut hADLType testmodel.teststructure.Obj2]
			steps (
				Sequential name Child1
				 bindings 
				 	[childAgent < agent ]
				 	[result > parentResult]
				 Parallel name Child2
				 bindings 
				 	[childAgent1 < agent ]
				 	[childInOut <> inOut]
				 steps (
					 Try name Child21
					 bindings 
					 	[childAgent2 < childAgent1 ]
					 
					 Choice name Child22
					 bindings 
					 	[childAgent3 < childAgent1 ]
					 
				 )
			)
		end
	)
		'''.
			parse
		val result = validateAgainstSchema(model, "ljbindings")
		val ref = XmlFileReader.read("/littlejil/ljbindings.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(result))
	}

	@Test
	def void testExceptionPropagation() {
		val model = '''
package exceptionPropagation
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Message name ExceptionType1
				Component name Comp1 actions
						[a1 type {C R U D}]
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name HandlerStep
			handlers
				[name SeqHandler hADLType testmodel.teststructure.ExceptionType1 action Continue]
			interfaces  
				[Resource agent hADLType testmodel.teststructure.Comp1]
			steps (
				Sequential name PropExStep steps (
					Sequential name ThrowStep throws [SeqExc handeld by SeqHandler]
				)
			)
		end
	)
'''.
			parse
		val result = validateAgainstSchema(model, "exceptionPropagation")
		val ref = XmlFileReader.read("/littlejil/exceptionPropagation.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(result))
	}

	@Test
	def void testImplicitAgent() {
		val model = '''
package implicitAgent
models
	( 	name testmodel
		structures
			name teststructure
			elements
				Artifact name Agent
			end
		end
	)
	( 	name ljmodel
		rootsteps
			Parallel name HandlerStep
			interfaces
				[Resource agent hADLType testmodel.teststructure.Agent]
			steps (
				Sequential name ImplicitAgentStep1 
				steps (
					Sequential name ImplicitAgentStep2
				)
			)
		end
	)
'''.
			parse
		val result = validateAgainstSchema(model, "implicitAgent")
		val ref = XmlFileReader.read("/littlejil/implicitAgent.xml")
		Assert::assertEquals(DslXmlFormatter.prettyFormat(ref), DslXmlFormatter.prettyFormat(result))
	}

}
