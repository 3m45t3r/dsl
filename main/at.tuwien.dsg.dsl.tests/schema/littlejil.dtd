<!--
    Global attributes:
    id: an identifier that can be used to associate metadata with the element
-->

<!--
    Root element.
-->
<!ELEMENT littlejil ( module, metadata? ) >

<!--
    Module declaration.
-->

<!ELEMENT module ( import | export | step-declaration | step-reference )* >
<!ATTLIST module id ID #IMPLIED >

<!--
    Declare a step as being imported into this module.
    step-name: the name of the step to be imported
-->
<!ELEMENT import EMPTY >
<!ATTLIST import step-name CDATA #REQUIRED
                 id ID #IMPLIED >

<!--
    Declare a step as being exported from this module.
    step-name: the name of the step to export
-->
<!ELEMENT export EMPTY >
<!ATTLIST export step-name CDATA #REQUIRED
                 id ID #IMPLIED >

<!--
    Container to hold external objects.
    encoding: string to document the encoding format for the object:
		base64 - base64 encoded java serialization format
		odesc  - XML encoded object descriptor
		string - character string
-->
<!ELEMENT external-object ( #PCDATA | aggregate )* >
<!ATTLIST external-object encoding ( base64 | odesc | string ) #REQUIRED
	                      id ID #IMPLIED >

<!--
    A step reference.
    target: the name of the step to reference.
-->
<!ELEMENT step-reference EMPTY >
<!ATTLIST step-reference target CDATA #REQUIRED
                         id ID #IMPLIED >

<!--
    A step declaration.
    name: the step name
    kind: the step kind
-->
<!ELEMENT step-declaration ( interface-decl | connector | aborter )* >
<!ATTLIST step-declaration name CDATA #REQUIRED
                           kind ( sequential | parallel | choice | try | leaf ) #REQUIRED
                           id ID #IMPLIED >

<!--
    A declaration for an entity in a step's interface.
    kind: the declaration kind
    name: the declaration name (if any)
-->
<!ELEMENT interface-decl ( external-object? ) >
<!ATTLIST interface-decl kind ( resource | resource-use | resource-collection | resource-collection-iterator |
                                resource-collection-use | in-parameter | out-parameter | in-out-parameter |
                                local-parameter | message | exception | channel ) #REQUIRED
	                     name CDATA #IMPLIED
	                     id ID #IMPLIED >

<!--
    A connector between two steps.
-->
<!ELEMENT connector ( ( requisite-connector | simple-requisite-connector | substep-connector | reaction-connector | handler-connector ),
                      binding*, ( step-declaration | step-reference )? ) >
<!ATTLIST connector id ID #IMPLIED >

<!--
    An aborter.
-->
<!ELEMENT aborter ( external-object ) >

<!--
    A requisite connector.
    pre-or-post: is this the connector to the pre- or post- requisite.
-->
<!ELEMENT requisite-connector EMPTY >
<!ATTLIST requisite-connector pre-or-post ( pre | post ) #REQUIRED >

<!--
    A simple requisite connector.
    pre-or-post: is this the connector to the pre- or post- requisite.
-->
<!ELEMENT simple-requisite-connector ( predicate-expression?, external-object? ) >
<!ATTLIST simple-requisite-connector pre-or-post ( pre | post ) #REQUIRED >

<!--
    Predicate expression
    expression: the expression
-->
<!ELEMENT predicate-expression EMPTY >
<!ATTLIST predicate-expression expression CDATA #REQUIRED >

<!--
    A substep connector.
-->
<!ELEMENT substep-connector ( cardinality? ) >

<!--
    Cardinality declaration
    lower-bound: minimum number of instances
    upper-bound: maximum number of instances
-->
<!ELEMENT cardinality ( (control-parameter | predicate-expression )? ) >
<!ATTLIST cardinality lower-bound CDATA #REQUIRED
                      upper-bound CDATA #REQUIRED
                      id ID #IMPLIED >

<!--
    Cardinality control parameter
    parameter-name: the name of the controlling parameter
-->
<!ELEMENT control-parameter EMPTY >
<!ATTLIST control-parameter parameter-name CDATA #REQUIRED >

<!--
    A reaction connector.
    parameter-name: name of a parameter to hold the message object
-->
<!ELEMENT reaction-connector ( external-object? ) >
<!ATTLIST reaction-connector parameter-name CDATA #IMPLIED >

<!--
    A handler connector.
    continuation-action: continuation action after the handler completes
    parameter-name: name of a parameter to hold exception object
-->
<!ELEMENT handler-connector ( external-object? ) >
<!ATTLIST handler-connector continuation-action ( complete | continue | restart | rethrow ) #REQUIRED
                            parameter-name CDATA #IMPLIED >

<!--
    Parameter binding.
    name-in-child: the name of the parameter in the child
-->
<!ELEMENT binding ( scope-binding | channel-binding | constant-binding ) >
<!ATTLIST binding name-in-child CDATA #REQUIRED
                  id ID #IMPLIED >

<!--
    A binding that associates a parameter with a parameter in the parent scope.
    kind: the direction of data flow
    name-in-parent: the name of the parameter in the parent
-->
<!ELEMENT scope-binding EMPTY >
<!ATTLIST scope-binding kind ( copy-in | copy-out | copy-in-and-out | constrain ) #REQUIRED
                        name-in-parent CDATA #REQUIRED >

<!--
    A binding that associates a parameter with a channel
    kind: the direction of data flow
    channel-name: the name of the channel
-->
<!ELEMENT channel-binding EMPTY>
<!ATTLIST channel-binding kind ( read | write | take | read-nowait | take-nowait ) #REQUIRED
                          channel-name CDATA #REQUIRED >

<!--
    A binding that associates a parameter with a constant
-->
<!ELEMENT constant-binding ( external-object? ) >

<!--
    Metadata section.
-->
<!ELEMENT metadata ( annotation | diagram )* >

<!--
    Annotation to a node.
    key: the id of the annotated node
    name: the annotation name
-->
<!ELEMENT annotation ( external-object ) >
<!ATTLIST annotation key IDREF #REQUIRED
                     name CDATA #REQUIRED >

<!--
    A diagram.
    name: the diagram name
-->
<!ELEMENT diagram ( post-it | location | tagged-location )* >
<!ATTLIST diagram name CDATA #REQUIRED
                  id ID #IMPLIED >

<!--
    A "post-it" in a diagram.
    x-position, y-position: the position of the post-it in the diagram
-->
<!ELEMENT post-it ( #PCDATA ) >
<!ATTLIST post-it x-position CDATA #REQUIRED
                  y-position CDATA #REQUIRED
                  id ID #IMPLIED >

<!--
    Location of a node in the diagram.
    key: the id of node to attach a location to
    x-position, y-position: the position of the node in the diagram
-->
<!ELEMENT location EMPTY >
<!ATTLIST location key IDREF #REQUIRED
                   x-position CDATA #REQUIRED
                   y-position CDATA #REQUIRED
                   id ID #IMPLIED >

<!--
    Location of a callout in the diagram.
    key: the id of node to attach a tagged-location to
    tag: the key
    x-position, y-position: the position of the callout in the diagram
-->
<!ELEMENT tagged-location EMPTY >
<!ATTLIST tagged-location key IDREF #REQUIRED
                          tag CDATA #REQUIRED
                          x-position CDATA #REQUIRED
                          y-position CDATA #REQUIRED
                          id ID #IMPLIED >

<!--
	Aggregate descriptor.
	type: the name of the type in the target object model
-->
<!ELEMENT aggregate ( field )* >
<!ATTLIST aggregate type CDATA #REQUIRED >

<!--
	Null aggregate descriptor.
	type: the name of the type in the target object model
-->
<!ELEMENT null-aggregate EMPTY >
<!ATTLIST null-aggregate type CDATA #REQUIRED >

<!--
	Field descriptor. If kind="aggregate" then the contents must be an aggregate tag.
	otherwise, the contents should be parsable content corresponding to the kind.
	
	name: the name of the field
	kind: the kind of the field
-->
<!ELEMENT field ( #PCDATA | aggregate | null-aggregate )* >
<!ATTLIST field name CDATA #REQUIRED
                kind ( aggregate | boolean | integer | real | string | symbol ) #REQUIRED >