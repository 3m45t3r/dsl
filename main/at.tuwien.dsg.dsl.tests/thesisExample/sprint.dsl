package scrum
//DO http://confluence.jetbrains.com/display/IntelliJIDEA/Custom+Language+Support
models
(
name scrumSprint
structures
  name scrumRoles
  elements
	Component name ProductOwner
	  actions
		[addUserStory type {C}]
		[increatePriorityOfUserStory type {R U}]
		[decreatePriorityOfUserStory type {R U}]
		[removeUserStory type {D}]
		[discussUserStory type {R U}]
	Component name DevelopingTeam
	  actions
		[takeUserStory type {D}]
		[discussUserStory type {R U}]
	Connector name ScrumMaster
	  actions
		[manage type {C R U D}]
	Artifact name ProductBacklog
	  actions
		[addUserStory type {C}]
		[increatePriorityOfUserStory type {R U}]
		[decreatePriorityOfUserStory type {R U}]
		[removeUserStory type {D}]
	Artifact name SprintBacklog
	  actions
		[addUserStory type {C}]
		[removeUserStory type {D}]
	Stream name SprintPlanningMeeting
	  actions
		[manage type {C R U D}]
		[discussUserStory type {R U}]
	Link scrumSprint.scrumRoles.ProductOwner.addUserStory
		 : scrumSprint.scrumRoles.ProductBacklog.addUserStory
	Link scrumSprint.scrumRoles.ProductOwner.increatePriorityOfUserStory
		 : scrumSprint.scrumRoles.ProductBacklog.increatePriorityOfUserStory
	Link scrumSprint.scrumRoles.ProductOwner.decreatePriorityOfUserStory
		 : scrumSprint.scrumRoles.ProductBacklog.decreatePriorityOfUserStory
	Link scrumSprint.scrumRoles.ProductOwner.removeUserStory
		 : scrumSprint.scrumRoles.ProductBacklog.removeUserStory
	Link scrumSprint.scrumRoles.ProductOwner.addUserStory
		 : scrumSprint.scrumRoles.SprintBacklog.addUserStory
	Link scrumSprint.scrumRoles.DevelopingTeam.takeUserStory
		 : scrumSprint.scrumRoles.SprintBacklog.removeUserStory
	Link scrumSprint.scrumRoles.ScrumMaster.manage
		 : scrumSprint.scrumRoles.SprintPlanningMeeting.manage
	Link scrumSprint.scrumRoles.ProductOwner.discussUserStory
		 : scrumSprint.scrumRoles.SprintPlanningMeeting.discussUserStory
	Link scrumSprint.scrumRoles.DevelopingTeam.discussUserStory
		: scrumSprint.scrumRoles.SprintPlanningMeeting.discussUserStory
  end
end
)
(
name scrumProcess
rootsteps
  Sequential name Scrum
	interfaces
	  [Resource agent hADLType scrumSprint.scrumRoles.ScrumMaster]
	  [ParameterIn productBacklog hADLType scrumSprint.scrumRoles.ProductBacklog]
	  [ParameterIn sprintBacklog hADLType scrumSprint.scrumRoles.SprintBacklog]
  steps (
   Sequential name SprintPlaning
	interfaces
	  [Resource agent hADLType scrumSprint.scrumRoles.ScrumMaster]
	  [Resource sprintPlaningMeeting hADLType scrumSprint.scrumRoles.SprintPlanningMeeting]
	bindings
	  [productBacklog <> productBacklog]
	  [sprintBacklog <> sprintBacklog]
	steps (
		Leaf name ProductBacklogRefinement
		  bindings
			[productBacklog <> productBacklog]
		Leaf name SprintBacklogCreation
		  bindings
			[sprintBacklog <> sprintBacklog]
			[sprintPlaningMeeting < sprintPlaningMeeting]
	)
   Sequential name Sprint
	interfaces
	  [Resource agent hADLType scrumSprint.scrumRoles.DevelopingTeam]
	bindings
	  [sprintBacklog < sprintBacklog]
	steps (
	  Leaf name DevelopmentOfUserStories
		bindings
			[sprintBacklog < sprintBacklog]
	  Leaf name Release
	  Leaf name Deplyoment
	)
   Sequential name SprintReview
	interfaces
	  [Resource agent hADLType scrumSprint.scrumRoles.ScrumMaster]
   Sequential name SprintRetrospective
	interfaces
	  [Resource agent hADLType scrumSprint.scrumRoles.ScrumMaster]
)
end
)
end