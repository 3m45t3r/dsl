package at.tuwien.dsg.validation;

public enum ArchElementType {
	OBJECT, COLLABORATOR
}
