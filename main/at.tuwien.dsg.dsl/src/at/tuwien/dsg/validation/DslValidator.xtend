package at.tuwien.dsg.validation

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.Binding
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.CollaboratorType
import at.tuwien.dsg.dsl.DslPackage
import at.tuwien.dsg.dsl.HadlElement
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepInterface
import at.tuwien.dsg.dsl.SubStructure
import at.tuwien.dsg.dsl.Wire
import at.tuwien.dsg.generator.util.ImplicitInterfaceFactory
import java.util.HashSet
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.validation.CheckType

/**
 * Custom validation rules. 
 * 
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class DslValidator extends AbstractDslValidator {

//	private final static Logger LOG = Logger.getLogger(DslValidator);
	public static val DUPLICATE_PRIMITIVE = 'duplicate primitive'
	public static val INSUFFICIENT_PRIMITIVES = 'insufficient Primitives'
	public static val OVERPRIVISIONED_PRIMITIVES = 'overprovisioned Primitives'
	public static val MISSING_AGENT = "missing agent"
	public static val WRONG_ENDPOINT_TYPE = "wrong endpoint type"
	public static val TOO_MANY_MODELS = "to many models"
	public static val TOO_MANY_HADL_MODELS = "to many hADL models"
	public static val TOO_MANY_LJ_MODELS = "to many little-jil models"
	public static val OBJTO_WRONG_TYPE = "objTo wrong type"
	public static val OBJFROM_WRONG_TYPE = "objFrom wrong type"
	public static val UNEQ_RELATION_TYPE = "unequal relation type"
	public static val WRONG_WIRE_ACTION_TYPE = "wrong wire action type"
	public static val WIRE_ACTION_OUT_OF_SCOPE = "wire action out of scope"
	public static val ENUM2TYPE_MAP = #{ArchElementType.OBJECT -> ObjectType,
		ArchElementType.COLLABORATOR -> CollaboratorType}
	public static val ENUM_2_ACTION_MSG_MAP = #{ArchElementType.OBJECT ->
		"referenced action belongs not to an object typed element",
		ArchElementType.COLLABORATOR -> "referenced action belongs not to an collaborator typed element"}

	@Check(CheckType::NORMAL)
	def onlyOneHadlAndOneLittleJilModelPerFile(Model m) {
		if (m.models.size > 2) {
			val msg = "There are two many model declared. Only one Little-Jil and one hADL model are allowed per file!"
			error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_MODELS, "model")
		} else if (m.models.size == 2) {
			val m1 = m.models.get(0)
			val m2 = m.models.get(1)
			if (m1 instanceof HadlModel && m2 instanceof HadlModel) {
				val msg = "There are two hADL models defined. Only one hADL model is allowed per file!"
				error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_HADL_MODELS, "model")
			}
			if (m1 instanceof LittleJilModel && m2 instanceof LittleJilModel) {
				val msg = "There are two little-jil models defined. Only one little-jil model is allowed per file!"
				error(msg, DslPackage.Literals.MODEL__MODELS, TOO_MANY_LJ_MODELS, "model")
			}
		}
	}

	@Check(CheckType::NORMAL)
	/**
	 * LITTLE-JIL
	 * validation rule ensures that every root step has an agent. 
	 * the agent type must be InterfaceType.RESOURCE
	 */
	def agentDefintionInRootStep(Step s) {
		if (s.eContainer != null && s.eContainer instanceof Model) {

			// is a root step, so it needs an interface declaration agent
			for (si : s.interfaces) {
				if (si.name.equalsIgnoreCase("agent") && si.kind == InterfaceType.RESOURCE) {
					return;
				}
			}
			val msg = String.format("Root step %s needs to declare an Resource-interface with the name 'agent'", s.name)
			error(msg, DslPackage.Literals.STEP__INTERFACES, MISSING_AGENT, s.name)
		}
	}

	/**
	 * LITTLE-JIL
	 * if name of the right side of binding does not match any step-interface name in parents,
	 * no type can be derived for implicit step-interface declaration {@see ImplicitInterfaceFactory#getType(EObject eContainer, String name}
	 */
	@Check(CheckType::NORMAL)
	def bindingTypeDerivable(Binding b) {
		val parentStep = b.eContainer.eContainer;
		if (!findMatchingName(b.right, parentStep)) {
			val msg = String.format("Could not derive type for binding. Check if name %s exists in parent steps",
				b.right)
			error(msg, b, DslPackage.Literals.BINDING__RIGHT)
		}
	}

	/**
	 * checks if links between actions are compatible. 
	 */
	@Check(CheckType::NORMAL)
	def checkSameActionPrimitive(CollabLink link) {
		val collabActionErrorMsg = 'Collaborator Action has subset of primitives from Object Action'
		val collabLiteral = DslPackage.Literals::COLLAB_LINK__COLLAB_ACTION_ENDPOINT
		val objActionErrorMsg = 'Object Action has subset of primitives from Collaborator Action'
		val objLiteral = DslPackage.Literals::COLLAB_LINK__OBJ_ACTION_ENDPOINT

		actionCheck(link.collabActionEndpoint.types, link.objActionEndpoint.types, collabActionErrorMsg,
			objActionErrorMsg, collabLiteral, objLiteral, link.name)

		val collaborator = link.collabActionEndpoint.eContainer as ArchElement
		val object = link.objActionEndpoint.eContainer as ArchElement
		if (!(collaborator.type instanceof CollaboratorType)) {
			warning("CollabActionEndpoint " + link.collabActionEndpoint.name + " is not an action of a CollaboratorType",
				DslPackage.Literals::COLLAB_LINK__COLLAB_ACTION_ENDPOINT, DslValidator.WRONG_ENDPOINT_TYPE, link.name)
		}
		if (!(object.type instanceof ObjectType)) {
			warning("ObjActionEndpoint " + link.objActionEndpoint.name + " is not an action of an ObjectType",
				DslPackage.Literals::COLLAB_LINK__OBJ_ACTION_ENDPOINT, DslValidator.WRONG_ENDPOINT_TYPE, link.name)
		}
	}

	def actionCheck(EList<Primitive> fromTypes, EList<Primitive> toTypes, String fromErrorMsg, String toErrorMsg,
		EReference fromLiteral, EReference toLiteral, String name) {
		val isSubsetFrom = fromTypes.containsAll(toTypes);
		val isSubsetTo = toTypes.containsAll(fromTypes);
		if (isSubsetTo && !isSubsetFrom) {
			warning(fromErrorMsg, fromLiteral, OVERPRIVISIONED_PRIMITIVES, name)
		} else if (!isSubsetTo && ! isSubsetFrom) {
			warning(toErrorMsg, toLiteral, INSUFFICIENT_PRIMITIVES, name)
		}
	}

	/**
	 * checks if a primitive in an actions is used more than once
	 */
	@Check(CheckType::FAST)
	def duplicatePrimitives(Action a) {
		// for each name store the element of its first occurrence
		if (a.types != null && !a.types.empty) {
			// the set of duplicate names
			var duplicates = new HashSet<Primitive>();
			// iterate (once!) over all types in the model
			for (p : a.types) {
				if (!duplicates.add(p)) // duplicate	
					error("Duplicate Primitive " + p, a, DslPackage.Literals.ACTION__TYPES, DUPLICATE_PRIMITIVE);
			}
		}
	}

	def findMatchingName(String name, EObject pstep) {
		var step = pstep;
		while (step != null && step instanceof Step) {
			for (StepInterface si : (step as Step).interfaces) {
				if (si.name.equals(name)) {
					return true;
				}
			}
			for (Binding b : (step as Step).bindings) {
				if (b.left.equals(name)) {
					return true;
				}
			}
			step = step.eContainer;
		}
		return false
	}

	/**
	 * checks if relation is only between objects and that they have the same type
	 */
	@Check(CheckType::NORMAL)
	def checkRelation(Relation r) {
		val to = r.objTo as ArchElement
		val from = r.objFrom as ArchElement
		val toType = getEnumTypeOfArchElement(to)
		val fromType = getEnumTypeOfArchElement(from)
		if (toType != fromType) {
			warning(
				"element types differ. A relation should be between two collaborators or two objects",
				DslPackage.Literals::RELATION__NAME,
				DslValidator.UNEQ_RELATION_TYPE,
				r.name
			)
			return
		}
		if (from.type.eClass != to.type.eClass) {
			warning("Referenced elements are not from the exact same type.", DslPackage.Literals::RELATION__NAME,
				DslValidator.UNEQ_RELATION_TYPE, r.name)
			return
		}
	}

	@Check(CheckType::NORMAL)
	def checkActionReferences(Wire w) {
		val refType = getEnumTypeOfArchElementWhichContainsWire(w)
		if (!checkIfActionBelongsToRefType(w.collabActionEndpoint, refType,
			DslPackage.Literals::WIRE__COLLAB_ACTION_ENDPOINT)) {
			return;
		}
		if (!checkIfActionBelongsToRefType(w.objActionEndpoint, refType,
			DslPackage.Literals::WIRE__OBJ_ACTION_ENDPOINT)) {
			return;
		}

		val structureRef = (w.eContainer as SubStructure).structureRef
		var tmp = w as EObject
		while (!(tmp instanceof HadlStructure)) {
			tmp = tmp.eContainer
		}
		val structure = tmp as HadlStructure
		val availableActions = new HashSet<Action>
		availableActions.addAll(getActionInStructure(structureRef, refType))
		availableActions.addAll(getActionInStructure(structure, refType))

		if (!availableActions.contains(w.objActionEndpoint)) {
			val msg = String.format(
				"referenced action %s is not in scope. Only actions in structure %s and structure %s are valid.",
				w.objActionEndpoint.name, structure.name, structureRef.name)
			warning(msg, DslPackage.Literals::WIRE__OBJ_ACTION_ENDPOINT, DslValidator.WIRE_ACTION_OUT_OF_SCOPE,
				w.objActionEndpoint.name)
		}
		if (!availableActions.contains(w.collabActionEndpoint)) {
			val msg = String.format(
				"referenced action %s is not in scope. Only actions in structure %s and structure %s are valid.",
				w.collabActionEndpoint.name, structure.name, structureRef.name)
			warning(msg, DslPackage.Literals::WIRE__COLLAB_ACTION_ENDPOINT, DslValidator.WIRE_ACTION_OUT_OF_SCOPE,
				w.collabActionEndpoint.name)
		}
	}

	def getEnumTypeOfArchElement(ArchElement ae) {
		if (ObjectType.isAssignableFrom(ae.type.class)) {
			return ArchElementType.OBJECT
		} else if (CollaboratorType.isAssignableFrom(ae.type.class)) {
			return ArchElementType.COLLABORATOR
		}
		// this point should never be reached
		throw new RuntimeException("Unknown ArchElement type!")
	}

	def getEnumTypeOfArchElementWhichContainsWire(Wire wire) {
		val ae = (wire.eContainer.eContainer as ArchElement)
		return getEnumTypeOfArchElement(ae)
	}

	def checkIfActionBelongsToRefType(Action action, ArchElementType refType, EReference ref) {
		if (action.eContainer instanceof ArchElement) {
			val archElement = action.eContainer as ArchElement
			val refClass = ENUM2TYPE_MAP.get(refType)
			if (!(refClass.isAssignableFrom(archElement.type.class))) {
				warning(at.tuwien.dsg.validation.DslValidator.ENUM_2_ACTION_MSG_MAP.get(refType), ref,
					DslValidator.WRONG_WIRE_ACTION_TYPE, action.name)
				return false
			}
		}
		return true
	}

	def getActionInStructure(HadlStructure structure, ArchElementType refType) {
		val result = new HashSet<Action>
		for (HadlElement he : structure.elements) {
			if (he instanceof ArchElement) {
				var ae = he as ArchElement
				val refClass = ENUM2TYPE_MAP.get(refType)
				if (refClass.isAssignableFrom(ae.type.class)) {
					for (Action a : ae.actions) {
						result.add(a)
					}
				}
			}
		}
		result
	}

}
