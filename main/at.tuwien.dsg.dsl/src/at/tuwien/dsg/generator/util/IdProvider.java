package at.tuwien.dsg.generator.util;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import at.tuwien.dsg.dsl.Action;
import at.tuwien.dsg.dsl.ArchElement;
import at.tuwien.dsg.dsl.Model;

/**
 * Provides id's for XML elements in hAdl and LittleJil. Each dsl rule class has
 * its own id counter.
 * 
 * @author stefan
 *
 */
public class IdProvider {

	private static final Map<Class<? extends EObject>, Integer> idMap = new HashMap<Class<? extends EObject>, Integer>();
	private static int litjilId = 0;
	private static int wireid = 0;

	private IdProvider() {
	}

	public static String getLitJilId() {
		return "_" + Integer.toString(litjilId++);
	}
	
	public static String generatedWireId(){
		return "_" + Integer.toString(wireid++);
	}

}