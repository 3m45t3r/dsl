package at.tuwien.dsg.generator.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.common.types.JvmTypeReference;

import at.tuwien.dsg.dsl.ArchElement;
import at.tuwien.dsg.dsl.DslFactory;
import at.tuwien.dsg.dsl.ExceptionHandler;
import at.tuwien.dsg.dsl.HadlTypedExceptionHandler;
import at.tuwien.dsg.dsl.HadlTypedStepInterface;
import at.tuwien.dsg.dsl.InterfaceType;
import at.tuwien.dsg.dsl.JavaTypedExceptionHandler;
import at.tuwien.dsg.dsl.JavaTypedStepInterface;
import at.tuwien.dsg.dsl.Step;
import at.tuwien.dsg.dsl.StepException;
import at.tuwien.dsg.dsl.StepInterface;

public class LittleJilExceptionPropagator {
	private final static Logger LOG = Logger.getLogger(LittleJilExceptionPropagator.class);

	private LittleJilExceptionPropagator() {

	}

	public static void createImplicitException(Step s) {
		Set<StepInterface> implicitStepInterfaces = convert(searchForException(s));
		List<StepInterface> forTest = new ArrayList<StepInterface>();
		forTest.addAll(implicitStepInterfaces);
		s.getInterfaces().addAll(implicitStepInterfaces);
	}

	private static Set<StepInterface> convert(Set<StepException> exceptionsThrown) {
		Set<StepInterface> exceptionDeclarations = new HashSet<StepInterface>();
		for (StepException se : exceptionsThrown) {
			Object type = getType(se.getHandeldBy());
			if (type instanceof ArchElement) {
				ArchElement castedType = (ArchElement) type;
				HadlTypedStepInterface hsi = DslFactory.eINSTANCE.createHadlTypedStepInterface();
				hsi.setName(se.getName());
				hsi.setKind(InterfaceType.EXCEPTION);
				hsi.setType(castedType);
				exceptionDeclarations.add(hsi);
			} else if (type instanceof JvmTypeReference) {
				JvmTypeReference castedType = (JvmTypeReference) type;
				JavaTypedStepInterface jsi = DslFactory.eINSTANCE.createJavaTypedStepInterface();
				jsi.setName(se.getName());
				jsi.setKind(InterfaceType.EXCEPTION);
				jsi.setType(castedType);
				exceptionDeclarations.add(jsi);
			} else {
				LOG.error(String.format("Couldn't determine type for StepException %s", se.getName()));
			}
		}
		return exceptionDeclarations;
	}

	private static Object getType(ExceptionHandler handeldBy) {
		if (handeldBy instanceof HadlTypedExceptionHandler) {
			HadlTypedExceptionHandler heh = (HadlTypedExceptionHandler) handeldBy;
			return heh.getType();
		} else if (handeldBy instanceof JavaTypedExceptionHandler) {
			JavaTypedExceptionHandler jeh = (JavaTypedExceptionHandler) handeldBy;
			return jeh.getType();
		} else {
			LOG.error(String.format("Unkown type for ExceptionHandler %s. This should never happen!",
					handeldBy.getName()));
			return null;
		}
	}

	private static Set<StepException> searchForException(Step s) {
		List<StepException> exceptions = new ArrayList<StepException>();
		Set<ExceptionHandler> handlers = createStartSet(s.getExceptionHandlers());
		recurisveSearch(s, exceptions, handlers);
		// remove already handled exceptions
		Set<StepException> unhandeledExceptions = new HashSet<StepException>();
		for (StepException e : exceptions) {
			if (!handlers.contains(e.getHandeldBy())) {
				unhandeledExceptions.add(e);
			}
		}
		return unhandeledExceptions;
	}

	private static Set<ExceptionHandler> createStartSet(EList<ExceptionHandler> exceptionHandlers) {
		Set<ExceptionHandler> result = new HashSet<ExceptionHandler>();
		for (ExceptionHandler eh : exceptionHandlers) {
			result.add(eh);
		}
		return result;
	}

	private static void recurisveSearch(Step s, List<StepException> exceptions, Set<ExceptionHandler> handlers) {
		if (s == null) {
			LOG.error("step parameter was null. This should never happen!");
		} else {
			if (s.getSubsteps() != null && !s.getSubsteps().isEmpty()) {
				for (Step sub : s.getSubsteps()) {
					recurisveSearch(sub, exceptions, handlers);
				}
			} else {
				for (ExceptionHandler eh : s.getExceptionHandlers()) {
					handlers.add(eh);
				}
				for (StepException se : s.getStepExceptions()) {
					exceptions.add(se);
				}
			}
		}
	}
}
