package at.tuwien.dsg.generator.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.common.types.JvmTypeReference;

import at.tuwien.dsg.dsl.ArchElement;
import at.tuwien.dsg.dsl.Binding;
import at.tuwien.dsg.dsl.DslFactory;
import at.tuwien.dsg.dsl.HadlTypedStepInterface;
import at.tuwien.dsg.dsl.InBinding;
import at.tuwien.dsg.dsl.InOutBinding;
import at.tuwien.dsg.dsl.InterfaceType;
import at.tuwien.dsg.dsl.JavaTypedStepInterface;
import at.tuwien.dsg.dsl.OutBinding;
import at.tuwien.dsg.dsl.Step;
import at.tuwien.dsg.dsl.StepInterface;

public class ImplicitInterfaceFactory {
	public final static String IMPLICIT_TYPE_ERROR = "__UNKNOWN_TYPE__";
	private final static Logger LOG = Logger.getLogger(ImplicitInterfaceFactory.class);

	private ImplicitInterfaceFactory() {
	}

	private static final Map<String, Object> bindingTypeMap = new HashMap<String, Object>();

	/**
	 * clears the bindingTypeMap, so every model has its own type bindings
	 */
	public static void clearBindingTypeMap() {
		bindingTypeMap.clear();
	}

	/**
	 * for every binding a StepInterface is added to the interface list of the
	 * given step s
	 * 
	 * @param s
	 * @return
	 */
	public static void createInterfaces(Step s) {
		for (Binding b : s.getBindings()) {
			if (isAgentBinding(b) && agentInterfaceAlreadyPresent(s)) {
				// no agent interface is needed because its already present
			} else {
				s.getInterfaces().add(createInterface(b));
			}
		}
	}

	private static boolean agentInterfaceAlreadyPresent(Step s) {
		for(StepInterface si : s.getInterfaces()){
			if(si.getName().equalsIgnoreCase("agent")){
				return true;
			}
		}
		return false;
	}

	private static boolean isAgentBinding(Binding b) {
		return b.getLeft().equalsIgnoreCase("agent") && b.getRight().equalsIgnoreCase("agent");
	}

	/**
	 * creates to a given Binding b a corresponding interface declaration. to
	 * determine the type the AST is traversed
	 * 
	 * @param b
	 * @return
	 */
	private static StepInterface createInterface(Binding b) {
		Object type = getType(b.eContainer(), b.getRight());
		if (type instanceof ArchElement) {
			ArchElement castedType = (ArchElement) type;
			HadlTypedStepInterface hsi = DslFactory.eINSTANCE.createHadlTypedStepInterface();
			hsi.setKind(getKind(b));
			hsi.setName(b.getLeft());
			hsi.setType(castedType);
			return hsi;
		} else if (type instanceof JvmTypeReference) {
			JvmTypeReference castedType = (JvmTypeReference) type;
			JavaTypedStepInterface jsi = DslFactory.eINSTANCE.createJavaTypedStepInterface();
			jsi.setKind(getKind(b));
			jsi.setName(b.getLeft());
			jsi.setType(castedType);
			return jsi;
		} else {
			LOG.error(String.format(
					"From binding (%s : %s) resulting StepInterface has unknown type. This should never happen!",
					b.getLeft(), b.getRight()));
			return null;
		}
	}

	private static InterfaceType getKind(Binding b) {
		if (b instanceof InBinding) {
			return InterfaceType.PARAMETER_IN;
		} else if (b instanceof OutBinding) {
			return InterfaceType.PARAMETER_OUT;
		} else if (b instanceof InOutBinding) {
			return InterfaceType.PARAMETER_IN_OUT;
		} else {
			LOG.error(String.format("Binding (%s : %s) has unknown type. This should never happen!", b.getLeft(),
					b.getRight()));
			return null;
		}
	}

	private static Object getType(EObject eContainer, String name) {
		String nodeName = "unknown";
		for (EAttribute at : eContainer.eClass().getEAttributes()) {
			if (at.getName().equalsIgnoreCase("name")) {
				nodeName = (String) eContainer.eGet(at);
			}
		}

		while (eContainer != null) {
			// lookup if type was found earlier
			if (bindingTypeMap.containsKey(name)) {
				return bindingTypeMap.get(name);
			}
			if (eContainer instanceof Step) {
				Step s = (Step) eContainer;
				// name remapping for type lookup , parent-child-child-* type
				for (Binding b : s.getBindings()) {
					if (b.getLeft().equals(name) && bindingTypeMap.containsKey(b.getRight())) {
						bindingTypeMap.put(name, bindingTypeMap.get(b.getRight()));
						LOG.info(String.format("mapped %s to type of %s", name, b.getRight()));
					}
				}
				for (StepInterface id : s.getInterfaces()) {
					if (id.getName().equals(name)) {
						if (!bindingTypeMap.containsKey(name)) {
							// put type for early lookup
							bindingTypeMap.put(name, getTypeFromStepInterface(id));
						}
						return getTypeFromStepInterface(id);
					}
				}
			}
			eContainer = eContainer.eContainer();
		}
		LOG.warn(String.format("Could not determine type of binding %s in %s", name, nodeName));
		ArchElement ae = DslFactory.eINSTANCE.createArchElement();
		ae.setName(IMPLICIT_TYPE_ERROR);
		return ae;
	}

	private static Object getTypeFromStepInterface(StepInterface si) {
		if (si instanceof HadlTypedStepInterface) {
			HadlTypedStepInterface hsi = (HadlTypedStepInterface) si;
			return hsi.getType();
		} else if (si instanceof JavaTypedStepInterface) {
			JavaTypedStepInterface jsi = (JavaTypedStepInterface) si;
			return jsi.getType();
		} else {
			LOG.error("Unkown StepInterface type. This should never happen!");
			return null;
		}
	}

}