package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.Binding
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.ExceptionHandler
import at.tuwien.dsg.dsl.HadlTypedExceptionHandler
import at.tuwien.dsg.dsl.HadlTypedStepInterface
import at.tuwien.dsg.dsl.InBinding
import at.tuwien.dsg.dsl.InterfaceType
import at.tuwien.dsg.dsl.JavaTypedExceptionHandler
import at.tuwien.dsg.dsl.JavaTypedStepInterface
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.Step
import at.tuwien.dsg.dsl.StepException
import at.tuwien.dsg.dsl.StepInterface
import at.tuwien.dsg.dsl.StepType
import at.tuwien.dsg.dsl.StreamType
import at.tuwien.dsg.dsl.impl.InOutBindingImpl
import at.tuwien.dsg.dsl.impl.OutBindingImpl
import at.tuwien.dsg.generator.util.IdProvider
import at.tuwien.dsg.generator.util.ImplicitAgentBindingFactory
import at.tuwien.dsg.generator.util.ImplicitInterfaceFactory
import at.tuwien.dsg.generator.util.LittleJilExceptionPropagator
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.dsl.LittleJilModel
import at.tuwien.dsg.dsl.Cardinality

/**
 * LittleJil generator
 */
class LittleJilGenerator implements IGenerator {

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				fsa.generateFile(m.package + "-littlejil.xml", m.compile)
			} else {
				fsa.generateFile(m.hashCode + "-littlejil.xml", m.compile)
			}
		}
	}

	def compile(Model m) '''
		«ImplicitInterfaceFactory.clearBindingTypeMap»
		«FOR model : m.models»
			«IF model instanceof LittleJilModel»
				«model.compile»
			«ENDIF»
		«ENDFOR»
	'''

	def compile(LittleJilModel lm) '''
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE littlejil PUBLIC "-//LASER//DTD Little-JIL 1.5//EN" "http://laser.cs.umass.edu/dtd/littlejil-1.5.dtd">
<littlejil>
	<module>
		«FOR s : lm.steps»
			«s.compile»
		«ENDFOR»
	</module>
</littlejil>
	'''

	def compile(Binding b) '''
		<binding name-in-child="«b.left»">
				<scope-binding kind="«bindingKind(b)»" name-in-parent="«b.right»" />
		</binding>
	'''

	def compile(Step s) '''
		«ImplicitAgentBindingFactory.createAgentBinding(s)»
		«ImplicitInterfaceFactory.createInterfaces(s)»
		«LittleJilExceptionPropagator.createImplicitException(s)»
		«FOR b : s.bindings»
			«b.compile»
		«ENDFOR»
		«IF s.kind != StepType.TASK_REFERENCE»
			<step-declaration id="«IdProvider.getLitJilId»" kind="«s.kind.toString.toLowerCase»" name="«s.name»">
				«FOR si : s.interfaces»
					«si.compile»
				«ENDFOR»
				«FOR ex : s.stepExceptions»
					«ex.compile»
				«ENDFOR»
				«IF !s.substeps.empty»
					«FOR su : s.substeps»
						<connector id="«IdProvider.getLitJilId»">
							«IF su.cardinality == null»
								<substep-connector />
							«ELSE»
								<substep-connector>
									<cardinality lower-bound="«su.cardinality.lowerBound»" upper-bound="«upperBound(su.cardinality)»" />
								</substep-connector>
							«ENDIF»
							«su.compile»
						</connector>
					«ENDFOR»
				«ENDIF»
				«FOR exH : s.exceptionHandlers»
					«exH.compile»
				«ENDFOR»
			</step-declaration>
		«ELSE»
			<step-reference id="«IdProvider.getLitJilId»" target="«s.name»"/>
		«ENDIF»
	'''

	def compile(StepException ex) '''
		<interface-decl kind="exception" name="«ex.name»">
			<external-object encoding="odesc">
				<aggregate type="«exceptionHandlerType(ex.handeldBy)»" />
			</external-object>
		</interface-decl>
	'''

	def compile(ExceptionHandler eh) '''
		<connector id="«IdProvider.getLitJilId»">
		<handler-connector continuation-action="«eh.action.toString.toLowerCase»" parameter-name="«eh.name»">
			<external-object encoding="odesc">
			       <aggregate type="«exceptionHandlerType(eh)»"/>
			   </external-object>
		</handler-connector>
		</connector>
	'''

	def compile(StepInterface si) '''
		<interface-decl kind="«interfaceKind(si.kind)»" name="«si.name»">
			<external-object encoding="odesc">
				<aggregate type="«stepInterfaceType(si)»" />
			</external-object>
		</interface-decl>
	'''

	/** upper bound * is in fact the value 2147483647 in little-jil */
	def String upperBound(Cardinality c) {
		if ('*'.equals(c.upperBound)) {
			"2147483647"
		} else {
			c.upperBound
		}
	}

	def String interfaceKind(InterfaceType ityp) {
		switch ityp {
			case LOCAL: "local-parameter"
			case PARAMETER_IN: "in-parameter"
			case PARAMETER_OUT: "out-parameter"
			case PARAMETER_IN_OUT: "in-out-parameter"
			case RESOURCE: "resource"
			case EXCEPTION: "exception"
			default: ""
		}
	}

	def String exceptionHandlerType(ExceptionHandler eh) {
		if (eh instanceof HadlTypedExceptionHandler) {
			return archElementType((eh as HadlTypedExceptionHandler).type.type)
		} else if (eh instanceof JavaTypedExceptionHandler) {
			return (eh as JavaTypedExceptionHandler).type.qualifiedName
		}
	}

	def String stepInterfaceType(StepInterface si) {
		if (si instanceof HadlTypedStepInterface) {
			return archElementType((si as HadlTypedStepInterface).type.type)
		} else if (si instanceof JavaTypedStepInterface) {
			return (si as JavaTypedStepInterface).type.qualifiedName
		}
	}

	def dispatch String archElementType(ComponentType t) {
		"Component"
	}

	def dispatch String archElementType(ConnectorType t) {
		"Connector"
	}

	def dispatch String archElementType(ArtifactType t) {
		"Artifact"
	}

	def dispatch String archElementType(MessageType t) {
		"Message"
	}

	def dispatch String archElementType(RequestType t) {
		"Request"
	}

	def dispatch String archElementType(StreamType t) {
		"Stream"
	}

	def dispatch String bindingKind(InBinding b) {
		"copy-in"
	}

	def dispatch String bindingKind(OutBindingImpl b) {
		"copy-out"
	}

	def dispatch String bindingKind(InOutBindingImpl b) {
		"copy-in-and-out"
	}

}
