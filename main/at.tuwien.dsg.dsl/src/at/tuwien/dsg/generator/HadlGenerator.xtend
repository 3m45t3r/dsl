package at.tuwien.dsg.generator

import at.tuwien.dsg.dsl.Action
import at.tuwien.dsg.dsl.ActivityScope
import at.tuwien.dsg.dsl.ArchElement
import at.tuwien.dsg.dsl.ArtifactType
import at.tuwien.dsg.dsl.CollabLink
import at.tuwien.dsg.dsl.ComponentType
import at.tuwien.dsg.dsl.ConnectorType
import at.tuwien.dsg.dsl.Contains
import at.tuwien.dsg.dsl.Depends
import at.tuwien.dsg.dsl.HadlModel
import at.tuwien.dsg.dsl.HadlStructure
import at.tuwien.dsg.dsl.Inherits
import at.tuwien.dsg.dsl.MessageType
import at.tuwien.dsg.dsl.Model
import at.tuwien.dsg.dsl.ObjectType
import at.tuwien.dsg.dsl.Primitive
import at.tuwien.dsg.dsl.References
import at.tuwien.dsg.dsl.Relation
import at.tuwien.dsg.dsl.RequestType
import at.tuwien.dsg.dsl.StreamType
import at.tuwien.dsg.dsl.SubStructure
import at.tuwien.dsg.dsl.Wire
import at.tuwien.dsg.generator.util.IdProvider
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

/**
 * hadl generator
 */
class HadlGenerator implements IGenerator {
	var String modelId

	override doGenerate(Resource input, IFileSystemAccess fsa) {
		for (m : input.allContents.toIterable.filter(Model)) {
			if (m.package != null && !m.package.empty) {
				modelId = m.package
			} else {
				modelId = m.hashCode.toString
			}
			fsa.generateFile(modelId + "-hADL.xml", m.compile)
		}

	}

	def compile(Model m) '''
		«FOR model : m.models»
			«IF model instanceof HadlModel»
				«model.compile»
			«ENDIF»
		«ENDFOR»
	'''

	def compile(HadlModel hm) '''
		<?xml version="1.0" encoding="UTF-8" standalone="no"?>
			<hADLmodel xmlns="http://at.ac.tuwien.dsg/hADL/hADLcore" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
				<name>«hm.name»</name>
				<description />
				<extension />
				«FOR hs : hm.structures»
					«hs.compile»
				«ENDFOR»
			</hADLmodel>
	'''

	def compile(HadlStructure hs) '''
		<hADLstructure id="«modelId».«id(hs)».«hs.name»">
			    <name>«hs.name»</name>
			    <description />
			    <extension />
				«FOR e : hs.elements»
					«IF e instanceof ArchElement»
						«(e as ArchElement).compile»
					«ELSEIF e instanceof CollabLink»
						«(e as CollabLink).compile»
					«ENDIF»
				«ENDFOR»
				«FOR r : hs.relations»
					«r.compile»
				«ENDFOR»
				«FOR c : hs.activityScopes»
					«c.compile»
				«ENDFOR»
		</hADLstructure>
	'''

	def compile(ActivityScope ac) '''
		<activityScope>
			<cardinality lowerBound="«ac.cardinality.lowerBound»" upperBound="«ac.cardinality.upperBound»"/>
			«FOR cr : ac.collaboratorRefs»
				<collaboratorRef>«modelId».«id(cr)».«cr.name»</collaboratorRef>
			«ENDFOR»
			«FOR or : ac.objectRefs»
				<objectRef>«modelId».«id(or)».«or.name»</objectRef>
			«ENDFOR»
			«FOR lr : ac.linkRefs»
				<linkRef>«modelId».«id(lr)».«lr.name»</linkRef>
			«ENDFOR»
		</activityScope>
	'''

	def compile(CollabLink l) '''
		«val objAE = l.objActionEndpoint»
		«val collAE = l.collabActionEndpoint»
		«val obj = objAE.eContainer as ArchElement»
		«val idObjPrefix = id(obj) + "." + obj.name»
		«val collab = collAE.eContainer as ArchElement»
		«val idCollabPrefix = id(collab) + "." + collab.name»
		«IF l.name != null»
		<link id="«modelId».«id(l)».«l.name»">
		«ELSE»
		<link>
		«ENDIF»
				<name>«l.name»</name>
				<objActionEndpoint>«modelId».«idObjPrefix».«id(objAE)».«objAE.name»</objActionEndpoint>
				<collabActionEndpoint>«modelId».«idCollabPrefix».«id(collAE)».«collAE.name»</collabActionEndpoint>
		</link>
	'''

	def compile(ArchElement ae) '''
		«startTag(ae.type)»id="«modelId».«id(ae)».«ae.name»"«typeAttribute(ae)»>
			<name>«ae.name»</name>
			<description />
			<extension />
				«FOR a : ae.actions»
					«a.compile»
				«ENDFOR»
				«IF ae.substructure != null»
					«ae.substructure.compile»
				«ENDIF»
		«endTag(ae.type)»
	'''

	def compile(Action a) '''
		<action id="«modelId».«id(a.eContainer)».«(a.eContainer as ArchElement).name».«id(a)».«a.name»">
		   <name>«a.name»</name>
		   <description />
		   <extension />
		   «FOR p : a.types»
		   	«primitive(p)»
		   «ENDFOR»
		   «IF a.cardinality != null»
		   	<cardinality lowerBound="«a.cardinality.lowerBound»" upperBound="«a.cardinality.upperBound»" />
		   «ENDIF»
		 </action>
	'''

	def compile(Relation r) '''
		<objectRef id="«modelId».«id(r)».«r.name»" xsi:type="tObjectCompositionRef">
			<name>«r.name»</name>
			<objFrom>«modelId».«id(r.objFrom)».«r.objFrom.name»</objFrom>
			<objTo>«modelId».«id(r.objTo)».«r.objTo.name»</objTo>
			<compositionType>«relationType(r.type)»</compositionType>
		</objectRef>
	'''

	def compile(SubStructure sub) '''
		«IF sub.isVirtual == null»
			<substructure>
		«ELSE»
			<substructure isVirtual="«sub.isVirtual»">
		«ENDIF»
			«FOR w : sub.wires»
			«w.compile»
			«ENDFOR»
			<substructureRef>«modelId».«id(sub.structureRef)».«sub.structureRef.name»</substructureRef>
		</substructure>
	'''

	def compile(Wire w) '''
		«val objAE = w.objActionEndpoint»
		«val collAE = w.collabActionEndpoint»
		«val obj = objAE.eContainer as ArchElement»
		«val idObjPrefix = id(obj) + "." + obj.name»
		«val collab = collAE.eContainer as ArchElement»
		«val idCollabPrefix = id(collab) + "." + collab.name»
		«IF w.name != null»
		<substructureWire id="«modelId».«id(w)».«w.name»">
			<name>«w.name»</name>
		«ELSE»
		<substructureWire id="«modelId».«id(w)».«IdProvider.generatedWireId»">
			<name/>
		«ENDIF»
			<objActionEndpoint>«modelId».«idObjPrefix».«id(objAE)».«objAE.name»</objActionEndpoint>
			<collabActionEndpoint>«modelId».«idCollabPrefix».«id(collAE)».«collAE.name»</collabActionEndpoint>
		</substructureWire>
	'''

	def String primitive(Primitive p) {
		switch p {
			case C: '<primitive>CREATE</primitive>'
			case R: '<primitive>READ</primitive>'
			case U: '<primitive>UPDATE</primitive>'
			case D: '<primitive>DELETE</primitive>'
			default: ""
		}
	}

	/**
	 * type attribute nur bei konkreter Klasse tSimpleCollabObject gefunden -> somit nur für ObjectType relevant
	 */
	def typeAttribute(ArchElement ae) {
		if (ae.type instanceof ObjectType) {
			val t = ae.type as ObjectType
			return " type=\"" + xsdType(t) + "\" xsi:type=\"tSimpleCollabObject\""
		}
		return ""
	}

	def dispatch String xsdType(RequestType t) {
		"REQUEST"
	}

	def dispatch String xsdType(StreamType t) {
		"STREAM"
	}

	def dispatch String xsdType(ArtifactType t) {
		"ARTIFACT"
	}

	def dispatch String xsdType(MessageType t) {
		"MESSAGE"
	}

	def dispatch String startTag(ComponentType t) {
		"<component "
	}

	def dispatch String startTag(RequestType t) {
		"<object "
	}

	def dispatch String startTag(StreamType t) {
		"<object "
	}

	def dispatch String startTag(ArtifactType t) {
		"<object "
	}

	def dispatch String startTag(MessageType t) {
		"<object "
	}

	def dispatch String startTag(ConnectorType t) {
		"<connector "
	}

	def dispatch String endTag(ComponentType t) {
		"</component>"
	}

	def dispatch String endTag(RequestType t) {
		"</object>"
	}

	def dispatch String endTag(StreamType t) {
		"</object>"
	}

	def dispatch String endTag(ArtifactType t) {
		"</object>"
	}

	def dispatch String endTag(MessageType t) {
		"</object>"
	}

	def dispatch String endTag(ConnectorType t) {
		"</connector>"
	}

	def dispatch String relationType(Inherits r) {
		"TO_INHERITS_FROM"
	}

	def dispatch String relationType(Contains r) {
		"FROM_CONTAINS_TO"
	}

	def dispatch String relationType(Depends r) {
		"FROM_DEPENDS_ON_TO"
	}

	def dispatch String relationType(References r) {
		"FROM_REFERENCES_TO"
	}

	def dispatch String id(Model o) {
		"model"
	}

	def dispatch String id(Action o) {
		"action"
	}

	def dispatch String id(ArchElement o) {
		"archElement"
	}
	
	def dispatch String id(Relation r){
		"relation"
	}

	def dispatch String id(CollabLink o) {
		"collabLink"
	}

	def dispatch String id(HadlStructure o) {
		"structure"
	}

	def dispatch String id(Wire o) {
		"wire"
	}
}
