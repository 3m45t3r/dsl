package at.tuwien.dsg.generator

import javax.inject.Inject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import at.tuwien.dsg.DslRuntimeModule

/**
 * main dsl generator. 
 * configured in @see at.tuwien.dsg.DslRuntimeModule#bindIGenerator()
 */
class DslGenerator implements IGenerator {

	@Inject HadlGenerator hadlGen
	@Inject LittleJilGenerator litjilGen

	/**
	 * starts hadl and littlejil generator.
	 */
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		hadlGen.doGenerate(input, fsa)
		litjilGen.doGenerate(input, fsa)
	}

}
