<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<!-- Identity transform -->
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="module">
		<xsl:copy-of select="." />
		<metadata>
			<diagram name="New Diagram">
				<xsl:for-each select="//step-declaration">
					<location>
						<xsl:attribute name="key">
                            <xsl:value-of select="@id" />
                        </xsl:attribute>
						<xsl:attribute name="x-position">
                            <xsl:value-of select="(position() * 100) mod 300" />
                        </xsl:attribute>
						<xsl:attribute name="y-position">
                            <xsl:value-of select="position() * 75" />
                        </xsl:attribute>
					</location>
				</xsl:for-each>
				<xsl:for-each select="//connector">
					<location>
						<xsl:attribute name="key">
                            <xsl:value-of select="@id" />
                        </xsl:attribute>
						<xsl:attribute name="x-position">
                            <xsl:value-of select="(position() * 100) mod 300" />
                        </xsl:attribute>
						<xsl:attribute name="y-position">
                            <xsl:value-of select="position() * 75" />
                        </xsl:attribute>
					</location>
				</xsl:for-each>
			</diagram>
		</metadata>
	</xsl:template>

</xsl:stylesheet>