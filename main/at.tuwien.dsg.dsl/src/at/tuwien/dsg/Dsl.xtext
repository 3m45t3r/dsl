grammar at.tuwien.dsg.Dsl with org.eclipse.xtext.xbase.Xbase

generate dsl "http://www.tuwien.at/dsg/Dsl"

//*********************** common ***********************
Model:
	('package' package=QualifiedName)?
	('import' imports+=FQNWithWildcard)*
	'models' (models+=DslModel)+
    ;

DslModel:
	HadlModel | LittleJilModel
;

FQN:
	ID ('.' ID)*;

FQNWithWildcard:
	FQN '.*'?;

Cardinality:
	lowerBound=(NUMBERS|'*') ',' upperBound=(NUMBERS|'*')
;

terminal NUMBERS:
	('0'..'9')+
;
//*********************** hadl ***********************
HadlModel:
	'(' 
		'name' name=FQN
		'structures' (structures+=HadlStructure)+
		'end'
	')'
;

HadlStructure:
	'name' name=FQN
	'elements' (elements+=HadlElement)+
	'end'
	('relations' (relations+=Relation)+
	'end')?
	('activityScopes' (activityScopes+=ActivityScope)+
	'end')?
;

HadlElement:
	ArchElement | CollabLink;

CollabLink:
	'Link' ('name' name=FQN)?
	objActionEndpoint=[Action|FQN] ':' collabActionEndpoint=[Action|FQN]
	;

ActivityScope:
	'['
		('links' (linkRefs+=[CollabLink|FQN])+)?
		('collaborators' (collaboratorRefs+=[ArchElement|FQN])+)?
		('objects' (objectRefs+=[ArchElement|FQN])+)?
		'cardinality' cardinality=Cardinality
	']' 
;

ArchElement:
	type=ArchElementType
	'name' name=FQN
	('actions' (actions+=Action)+)?
	('substructure' substructure=SubStructure)?
	;
	
SubStructure:
	'reference' structureRef=[HadlStructure|FQN]
	('virtual' isVirtual=Boolean)?
	('wires' (wires+=Wire)+)?
;

Wire:
	'Wire' ('name' name=FQN)?
	objActionEndpoint=[Action|FQN] ':' collabActionEndpoint=[Action|FQN]
	;

ArchElementType:
	CollaboratorType | ObjectType
	;

CollaboratorType:
	ComponentType | ConnectorType
;

ComponentType:
	{ComponentType} 'Component'
;

ConnectorType:
	{ConnectorType} 'Connector'
;

ObjectType:
	MessageType | RequestType | StreamType | ArtifactType
	;

MessageType:
	{MessageType} 'Message'
;

RequestType:
	{RequestType} 'Request'
;

StreamType:
	{StreamType} 'Stream'
;

ArtifactType:
	{ArtifactType} 'Artifact'
;

Action:
	'[' name=FQN
	('type' '{' (types+=Primitive)+ '}')?
	('cardinality' cardinality=Cardinality)?
	']';

enum Primitive:
	C | R | U | D;

Boolean:
	'true' | 'false'
;

Relation:
	'[' 'name' name=FQN ':' objTo=[ArchElement|FQN] type=RelationType objFrom=[ArchElement|FQN] ']';

RelationType:
	Inherits | Contains | Depends | References
;

Inherits:
	{Inherits} 'inherits' 'from';

Contains:
	{Contains} 'contains';

Depends:
	{Depends} 'depends' 'on';

References:
	{References} 'references';

//*********************** little-jil ***********************
LittleJilModel:
	'(' 
		'name' name=FQN
		'rootsteps' (steps+=Step)+ 
		'end'
	')'
;

Step:
	kind=StepType
	'name' name=FQN
	('cardinality' cardinality=Cardinality)?
	('handlers' (exceptionHandlers+=ExceptionHandler)+)?
	('interfaces' (interfaces+=StepInterface)+)?
	('bindings' (bindings+=Binding)+)?
	('steps' '(' (substeps+=Step)+ ')')?
	('throws' (stepExceptions+=StepException)+)?;

ExceptionHandler:
	HadlTypedExceptionHandler | JavaTypedExceptionHandler
;

HadlTypedExceptionHandler:
	'['
	'name' name=FQN
	'hADLType' type=[ArchElement|FQN]
	'action' action=HandlerType
	']';
	
JavaTypedExceptionHandler:
	'['
	'name' name=FQN
	'jvmType' type=JvmTypeReference
	'action' action=HandlerType
	']';
	
Binding:
	'[' (InBinding | OutBinding | InOutBinding) ']';

InBinding:
	left=FQN '<' right=FQN;

OutBinding:
	left=FQN '>' right=FQN;

InOutBinding:
	left=FQN '<>' right=FQN;

StepException:
	'[' name=FQN
	('handeld by' handeldBy=[ExceptionHandler|FQN])?
	']';

StepInterface:
	HadlTypedStepInterface | JavaTypedStepInterface
;

HadlTypedStepInterface:
	'[' kind=InterfaceType
	name=FQN
	'hADLType' type=[ArchElement|FQN]
	']';
	
JavaTypedStepInterface:
	'[' kind=InterfaceType
	name=FQN
	'jvmType' type=JvmTypeReference
	']';

enum HandlerType:
	Complete | Continue | Rethrow | Restart;

enum InterfaceType:
	Resource | ParameterIn | ParameterOut | ParameterInOut | Local | Exception;

enum StepType:
	Sequential | Try | Choice | Parallel | Leaf | TaskReference;