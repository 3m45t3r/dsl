package at.tuwien.dsg.generator.util;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.Test;

import at.tuwien.dsg.dsl.Binding;
import at.tuwien.dsg.dsl.RootStep;
import at.tuwien.dsg.dsl.Step;


public class IdProviderTest extends TestCase {
	
	@Test
	public void testGetId(){
		Assert.assertEquals("_1", IdProvider.getId(Binding.class));
		Assert.assertEquals("_2", IdProvider.getId(Binding.class));
		Assert.assertEquals("_3", IdProvider.getId(Binding.class));
		Assert.assertEquals("_4", IdProvider.getId(Binding.class));
		Assert.assertEquals("_5", IdProvider.getId(Binding.class));
		
		
		Assert.assertEquals("_1", IdProvider.getId(RootStep.class));
		Assert.assertEquals("_2", IdProvider.getId(RootStep.class));
		
		Assert.assertEquals("_1", IdProvider.getId(Step.class));
		Assert.assertEquals("_3", IdProvider.getId(RootStep.class));
		Assert.assertEquals("_2", IdProvider.getId(Step.class));
	}
	
}
